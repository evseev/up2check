# up2check

* Check new versions of software used in Emzior ISO builds
* See full description in http://redmine.emzior/issues/5901

### How to run:

* `up2check --remote-dir ftp://ftp.emzior/iso/vmm-mpls_x86_64/ --local-dir /tmp/iso/ --html --output /tmp/1.html --arch-cache arch-cache.txt --map emzios2arch.txt`

### Notes:

* Downloaded files are not deleted! You should delete it manually!
* `--map` file must be precreated. Builtin rules are used when started without `--map ..`
* mapfile lines are containing two columns: REGEX for emzior package name, and replacement mask. It's recommended to use '^' and '$' in the first column.

### Requiremens:

* 7z (p7zip package)
* lftp
* perl JSON module
* perl AnyEvent::HTTP module
* perl Net::SSLeay module
* perl Sort::Versions module

### Installation on Ubuntu:

* `apt install lftp p7zip-full libnet-ssleay-perl libjson-perl libanyevent-http-perl libsort-versions-perl`

### External resources:

* https://www.archlinux.org/packages/
* https://aur.archlinux.org/
