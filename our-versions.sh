#!/bin/sh

test -n "$1" || { echo "Usage: $0 path-to-iso"; exit 1; }
test -s "$1" || { echo "Error: missing iso $1"; exit 1; }

ISO="$1"

7z l "$ISO" \
| awk '/repo\/dists\/.+\/Packages$/ { print $NF; }' \
| xargs -r 7z x -so "$ISO" \
| perl -ne '$pkg = $1 if /^Package: (.+)$/; print "$pkg $1\n" if /^Version: (.+)-\d+$/' \
| sort | uniq > our-versions.list

## END ##
