#!/usr/bin/perl

use strict;
use warnings;

use JSON;
use AnyEvent::HTTP;
use Data::Dumper;

my $cv = AnyEvent->condvar;

my %versions;

open F, $ARGV[0] or die "Cannot open $ARGV[0]: $!\n";
my @lines = <F>;
close F;
chomp @lines;
%versions = map { my @a = split; $a[0] => $a[1] } @lines;

my $done = 0;

sub parse_response {
	my ($data, $headers) = @_;
	$cv->send if ++$done >= 2 * @lines;

	$data                      or return;
	my $p = decode_json($data) or return;
	$p = $p->{results}         or return;
	$p = shift @$p             or return;

	#print Dumper($p),"\n";
	my $n = $p->{pkgname} || $p->{PackageBase};
	my $v = $p->{pkgver}  || $p->{Version};
	$v =~ s/-\d+$//;   # ..strip release part
	print "$n $v\n";
}

foreach my $pkgname (keys %versions) {
	my $n = $pkgname;
	$n =~ /^mprdaemon/ and do { $done += 2; next };
	$n =~ s/^python3-/python-/;
	$n =~ s/^libnetfilter-/libnetfilter_/;
	$n =~ s/^lm-sensors/lm_sensors/;

	http_get "https://www.archlinux.org/packages/search/json/?name=$n",  sub { parse_response @_ };
	http_get "https://aur.archlinux.org/rpc.php?v=5&type=info&arg[]=$n", sub { parse_response @_ };

	$cv->send if $done >= 2 * @lines;
}

$cv->recv;  # ..mainloop

## END ##
