#!/usr/bin/perl

use strict;
use warnings;

sub file2hash($) {
	open F, $_[0] or die "Cannot open $_[0]: $!\n";
	my @lines = <F>;
	close F;
	chomp @lines;
	my %pairs = map { my @a = split; $a[0] => $a[1] } @lines;
	\%pairs;
}

my $emzior = file2hash($ARGV[0]);
my $arch   = file2hash($ARGV[1]);

while(my ($name, $version) = each %$emzior) {
	next if $name =~ /^mprdaemon/;
	my $n = $name;
	$n =~ s/^python3-/python-/;
	$n =~ s/^libnetfilter-/libnetfilter_/;
	$n =~ s/^lm-sensors/lm_sensors/;

	$arch->{$n}              or do { print "UNKNOWN: $name\n"; next };
	$arch->{$n} eq $version and do { print "ACTUELL: $name $version\n"; next };
	                                 print "UPDATED: $name: $version $arch->{$n}\n";
}

## END ##
